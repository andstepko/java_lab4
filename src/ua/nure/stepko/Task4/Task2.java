package ua.nure.stepko.Task4;

import java.io.IOException;

public class Task2 {
	private static final String NEW_FILE_NAME = "newTable.CSV";
	
	private static Data data;
	
	public static void main(String[] args) throws IOException {
		String oldFileName = args[0];
		
		data = new Data(oldFileName);
		data.fixAllMarks();
		
		Data.createFillFile(NEW_FILE_NAME, data.toString());
		System.out.println(data);
	}
	
	public static String getFileName(){
		return NEW_FILE_NAME;
	}
}
