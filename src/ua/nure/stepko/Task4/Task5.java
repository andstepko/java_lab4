package ua.nure.stepko.Task4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task5 {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];
		Data data = new Data(fileName);
		
		int studNum = data.getStudentsNum();
		List<PairSLS> arentWritten = new ArrayList<PairSLS>();
		
		for(int i = 0; i < studNum; i++){
			arentWritten.add(new PairSLS(data.getStudentName(i), data.didntWriteStudent(i)));
		}
		// Sort.
		Collections.sort(arentWritten, PairSLS.getComparator());
		
		for(PairSLS pair : arentWritten){
			print(pair);
		}
	}
	
	private static void print(PairSLS pair){
		StringBuffer sBuf = new StringBuffer();
		List<String> value = pair.getValue();
		if(value == null){
			value = new ArrayList<String>();
		}
		
		sBuf.append(pair.getKey());
		sBuf.append("  ===> ");
		sBuf.append(value.size());
		if(value.size() > 0){
			sBuf.append(": [");
			for(int i = 0; i < value.size() - 1; i++){
				sBuf.append(value.get(i));
				sBuf.append(", ");
			}
			sBuf.append(value.get(value.size() - 1));
			sBuf.append("]");
		}
		System.out.println(sBuf.toString());
	}
}
