package ua.nure.stepko.Task4;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Data {
	private static final int NO_NUMBER = -1;
	private static final String NO_NUMBER_STRING = "";
	private static final String PARTS_SEPARATOR = ";";

	private String text;

	public Data(String fileName) throws IOException {
		Scanner scanner = new Scanner(new File(fileName), Charset.defaultCharset().name());
		StringBuffer result = new StringBuffer();

		while (scanner.hasNextLine()) {
			result.append(scanner.nextLine());
			result.append(System.lineSeparator());
		}
		scanner.close();
		text = result.toString();
	}

	public String toString() {
		return text;
	}

	public void saveToFile(String fileName) throws IOException {
		createFillFile(fileName, text);
	}

	public boolean fixAllMarks(){
		boolean result = true;
		
		for(int i = 0; i < getStudentsNum(); i++){
			result &= fixMarks(i);
		}		
		return result;
	}

	private boolean fixMarks(int studentIndex){
		int[] questionsNums = getQuestionsNums();
		int[] studentMarks = getStudentMarks(studentIndex);
		
		if(questionsNums.length != studentMarks.length){
			System.out.println("Error! (questionsNums.length != studentMarks.length)");
			return false;
		}
		
		for(int i = 0; i < studentMarks.length; i++){
			if(studentMarks[i] == NO_NUMBER){
				continue;
			}
			
			int newMark = getClosest(studentMarks[i], questionsNums[i]);
			studentMarks[i] = newMark;
		}
		setStudentMarks(studentIndex, studentMarks);
		return true;
	}
	
	private int[] getQuestionsNums(){
		return getNumbersFromLine(0);
	}
	
	public int getTestsNum(){
		return getLine(1).split(PARTS_SEPARATOR).length - 1;
	}

	public int getStudentsNum(){
		String[] lines = getLines();
		
		return lines.length - 2;
	}

	public int getAverageStudentMark(int studentIndex){
		int[] marks = getStudentMarks(studentIndex);
		int sum = 0;
		
		for(int i : marks){
			sum += i;
		}
		return (int) (sum / (double)marks.length); 
	}
	
	public int getAverageTestMark(int testIndex){
		if(testIndex >= getTestsNum()){
			return -2;
		}
		
		int studNum = getStudentsNum();
		int sum = 0;
		
		for(int i = 0; i < studNum; i++){
			int mark = getStudentMark(i, testIndex);
			if(mark != NO_NUMBER){
				sum += mark;
			}
		}
		return (int) (sum / (double)studNum);
	}
	
	private int getStudentMark(int studentIndex, int testIndex){
		int[] studentMarks = getStudentMarks(studentIndex);
		
		if(testIndex >= studentMarks.length){
			return -2;
		}
		return studentMarks[testIndex];
	}
	
	private int[] getStudentMarks(int studentIndex) {
		return getNumbersFromLine(studentIndex + 2);
	}

	private boolean setStudentMarks(int studentIndex, int[] marks){
		return setNumbersToLine(studentIndex + 2, marks);
	}
	
	public String getStudentName(int studentIndex){
		String studentLine = getLine(studentIndex + 2);
		String[] parts = studentLine.split(PARTS_SEPARATOR);
		
		if(parts.length == 0){
			return null;
		}
		return parts[0];
	}
	
	public String getTestName(int testIndex){
		String[] testNames = getLine(1).split(PARTS_SEPARATOR);
		
		if((testIndex < 0) || (testIndex >= testNames.length - 1)){
			return null;
		}
		return testNames[testIndex + 1];
	}
	
	public List<String> didntWriteTest(int testIndex){
		int studNum = getStudentsNum();
		List<String> result = new ArrayList<String>();
		
		for(int i = 0; i < studNum; i++){
			if(getStudentMark(i, testIndex) == NO_NUMBER){
				result.add(getStudentName(i));
			}
		}
		Collections.sort(result);
		return result;
	}
	
	public List<String> didntWriteStudent(int studentIndex){
		int testNum = getTestsNum();
		int[] marks = getStudentMarks(studentIndex);
		List<String> result = new ArrayList<String>();
		
		if(marks.length != testNum){
			return null;
		}
		
		for(int i = 0; i < testNum; i++){
			if(marks[i] == NO_NUMBER){
				result.add(getTestName(i));
			}
		}
		Collections.sort(result);
		return result;
	}
	// Low-level.
	private int[] getNumbersFromLine(int lineIndex) {
		String line = getLine(lineIndex);

		if (line == null) {
			return null;
		}

		String[] parts = line.split(PARTS_SEPARATOR);
		int[] result = new int[parts.length - 1];

		for (int i = 1; i < parts.length; i++) {
			if(parts[i].equals(NO_NUMBER_STRING)){
				result[i - 1] = NO_NUMBER;
				continue;
			}
			result[i - 1] = Integer.valueOf(parts[i]);
		}
		return result;
	}

	private boolean setNumbersToLine(int lineIndex, int[] numbers) {
		String line = getLine(lineIndex);

		if (line == null) {
			return false;
		}

		String[] parts = line.split(PARTS_SEPARATOR);
		StringBuffer newLineBuf = new StringBuffer();
		if (parts.length == 0) {
			return false;
		}
		newLineBuf.append(parts[0]);
		for (int i = 0; i < numbers.length; i++) {
			if(numbers[i] == NO_NUMBER){
				newLineBuf.append(PARTS_SEPARATOR);
				newLineBuf.append(NO_NUMBER_STRING);
				continue;
			}
			newLineBuf.append(PARTS_SEPARATOR);
			newLineBuf.append(numbers[i]);
		}
		setLine(lineIndex, newLineBuf.toString());
		return true;
	}

	private String getLine(int lineIndex) {
		String[] lines = getLines();

		if (lineIndex > lines.length) {
			return null;
		}
		return lines[lineIndex];
	}
	
	private boolean setLine(int lineIndex, String line) {
		String[] lines = getLines();

		if (lineIndex > lines.length) {
			return false;
		}

		lines[lineIndex] = line;
		setLines(lines);
		return true;
	}

	private String[] getLines() {
		return text.split(System.lineSeparator());
	}

	private void setLines(String[] lines) {
		StringBuffer sBuf = new StringBuffer();

		for (String s : lines) {
			sBuf.append(s);
			sBuf.append(System.lineSeparator());
		}
		text = sBuf.toString();
	}

	// Static methods. 
	private static int getClosest(int initialValue, int questionsNum){
		int[] availableMarks = getAvailableMarks(questionsNum);
		return getClosest(initialValue, availableMarks);
	}
	private static int getClosest(int initialValue, int[] patternValues) {
		int i = 0;
		for (; i < patternValues.length; i++) {
			if (initialValue <= patternValues[i]) {
				break;
			}
		}
		// patternValues[i] >= initialValue
		if (i == 0){
			// The first patternValue.
						return patternValues[i];
		}
		// Is not the first patternValue.
		if(initialValue > patternValues[patternValues.length - 1]){
			return patternValues[patternValues.length - 1];
		}
		
		int diffToSmaller = initialValue - patternValues[i-1];
		int diffToBigger = patternValues[i] - initialValue;

		if (diffToSmaller < diffToBigger) {
			return patternValues[i - 1];
		} else {
			// Closer to the bigger neighbor.
			return patternValues[i];
		}
	}
	
	private static int[] getAvailableMarks(int testsNum) {
		int[] result = new int[testsNum + 1];

		for (int i = 0; i < testsNum + 1; i++) {
			int availableMark = (int) Math.round(i * 100 / (double) testsNum);
			result[i] = availableMark;
		}
		return result;
	}

	public static void createFillFile(String fileName, String text)
			throws IOException {
		File file = new File(fileName);
		boolean b = file.createNewFile();
		if(!b){
			System.out.println("Error creating new file!");
		}
		PrintWriter pw = new PrintWriter(file, Charset.defaultCharset().name());
		pw.write(text);
		pw.close();
	}
}