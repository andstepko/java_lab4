package ua.nure.stepko.Task4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task6 {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];
		Data data = new Data(fileName);
		List<PairSI> resultList = new ArrayList<PairSI>();
		
		int testsNum = data.getTestsNum();
		for(int i = 0; i < testsNum; i++){
			resultList.add(new PairSI(data.getTestName(i), data.getAverageTestMark(i)));
		}
		// Sort.
		Collections.sort(resultList, PairSI.getComparator());
		
		for(PairSI pair : resultList){
			print(pair);
		}
	}
	
	private static void print(PairSI pair){
		StringBuffer sBuf = new StringBuffer();
		
		sBuf.append(pair.getKey());
		sBuf.append("  ===> ");
		sBuf.append(pair.getValue());
		System.out.println(sBuf.toString());
	}
}
