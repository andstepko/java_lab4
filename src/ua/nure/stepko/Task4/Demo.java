package ua.nure.stepko.Task4;

import java.io.IOException;

public class Demo {
	public static void main(String[] args) throws IOException {
		System.out.println("=======Part1=======");
		Task1.main(args);
		
		System.out.println("=======Part2=======");
		Task2.main(new String[] {Task1.getFileName()});
		
		System.out.println("=======Part3=======");
		Task3.main(new String[] {Task2.getFileName()});
		
		System.out.println("=======Part4=======");
		Task4.main(new String[] {Task2.getFileName()});
		
		System.out.println("=======Part5=======");
		Task5.main(new String[] {Task2.getFileName()});
		
		System.out.println("=======Part6=======");
		Task6.main(new String[] {Task2.getFileName()});
	}
}
