package ua.nure.stepko.Task4;

import java.util.Comparator;
import java.util.List;

public class PairSLS {
	
	private String key;
	private List<String> value;

	public PairSLS(String keyInput, List<String> valueInput){
		key = keyInput;
		value = valueInput;
	}

	public String getKey(){
		return key;
	}
	
	public void setKey(String keyInput){
		key = keyInput;
	}
	
	public List<String> getValue(){
		return value;
	}
	
	public void setValue(List<String> valueInput){
		value = valueInput;
	}
	
	public int getValueSize(){
		if(value == null){
			return 0;
		}
		return value.size();
	}
	
	public static Comparator<PairSLS> getComparator(){
		return new Comparator<PairSLS>(){

			@Override
			public int compare(PairSLS pair1, PairSLS pair2) {
				if(pair1.getValueSize() == pair2.getValueSize()){
					// Sort by name.
					return pair1.getKey().compareTo(pair2.getKey());
				}
				// Sort by size.
				return pair2.getValueSize() - pair1.getValueSize();
			}
		};
	}
}
