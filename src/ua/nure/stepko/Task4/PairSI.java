package ua.nure.stepko.Task4;

import java.util.Comparator;

public class PairSI {

	private String key;
	private int value;

	public PairSI(String keyInput, int valueInput){
		key = keyInput;
		value = valueInput;
	}

	public String getKey(){
		return key;
	}
	
	public void setKey(String keyInput){
		key = keyInput;
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int valueInput){
		value = valueInput;
	}
	
	public static Comparator<PairSI> getComparator(){
		return new Comparator<PairSI>(){

			@Override
			public int compare(PairSI pair1, PairSI pair2) {
				int value1 = pair1.getValue();
				int value2 = pair2.getValue();
				
				if(value1 == value2){
					// Sort by name.
					return pair1.getKey().compareTo(pair2.getKey());
				}
				// Sort by size.
				return value2 - value1;
			}
		};
	}
}
