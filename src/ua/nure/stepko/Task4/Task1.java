package ua.nure.stepko.Task4;

import java.io.IOException;
import java.util.Random;

public class Task1 {
	private static final String FILE_NAME = "table.CSV";
	private static final String PARTS_SEPARATOR = ";";
	private static final String TEST_NAME_FAMILY = "Test_";
	private static final String STUDENT_NAME_FAMILY = "Lastname_";

	public static void main(String[] args) throws IOException {
		int students = 5;
		int tests = 6;
		
		String tableText = composeTable(tests, students);
		Data.createFillFile(FILE_NAME, tableText);
		System.out.println(tableText);
	} 

	public static String getFileName(){
		return FILE_NAME;
	}
	
	private static String composeTable(int tests, int students) {
		StringBuffer sBuf = new StringBuffer();

		sBuf.append(composeFirstRow(tests));
		sBuf.append(System.lineSeparator());
		sBuf.append(composeSecondRow(tests));
		sBuf.append(System.lineSeparator());
		for (int i = 0; i < students; i++) {
			sBuf.append(composeFollowingRow(i, tests));
			sBuf.append(System.lineSeparator());
		}
		return sBuf.toString();
	}

	private static String composeFirstRow(int tests) {
		StringBuffer sBuf = new StringBuffer();

		for (int i = 0; i < tests; i++) {
			sBuf.append(PARTS_SEPARATOR);
			sBuf.append((int) ((Math.random() * 8) + 7));
		}
		return sBuf.toString();
	}

	private static String composeSecondRow(int tests) {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < tests; i++) {
			sb.append(PARTS_SEPARATOR);
			sb.append(composeTestName(i + 1));
		}
		return sb.toString();
	}

	private static String composeFollowingRow(int rowIndex, int tests) {
		StringBuffer sb = new StringBuffer();

		sb.append(composeStudentName(rowIndex));
		Random r = new Random();
		for (int i = 0; i < tests; i++) {
			int mark = r.nextInt(100);

			sb.append(PARTS_SEPARATOR);
			if (isSuitableMark(mark)) {
				sb.append(String.valueOf(mark));
			} else {
				// Leave empty.
			}
		}
		return sb.toString();
	}

	private static boolean isSuitableMark(int mark) {
		return mark % 5 > 0;
	}

	private static String composeTestName(int testNumber) {
		return TEST_NAME_FAMILY + testNumber;
	}

	private static String composeStudentName(int studentIndex) {
		return STUDENT_NAME_FAMILY + studentIndex;
	}
}