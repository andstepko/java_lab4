package ua.nure.stepko.Task4;

import java.io.IOException;

public class Task3 {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];
		Data data = new Data(fileName);
		
		for(int i = 0; i < data.getStudentsNum(); i++){
			System.out.println(data.getStudentName(i) + "  ===> " + data.getAverageStudentMark(i));
		}
		
	}
}
