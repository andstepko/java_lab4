package ua.nure.stepko.Task4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task4 {
	public static void main(String[] args) throws IOException {
		String fileName = args[0];
		Data data = new Data(fileName);
		
		int testsNum = data.getTestsNum();
		List<PairSLS> didntWrite = new ArrayList<PairSLS>();
		
		for(int i = 0; i < testsNum; i++){
			ArrayList<String> didntWriteTheTest = (ArrayList<String>) data.didntWriteTest(i);
			if(didntWriteTheTest.size() > 0){
				didntWrite.add(new PairSLS(data.getTestName(i), didntWriteTheTest));
			}
		}
		// Sort by number of students and then by alphabet.
		Collections.sort(didntWrite, PairSLS.getComparator());
	
		for(PairSLS pair : didntWrite){
			print(pair);
		}
	}
	
	private static void print(PairSLS pair){
		StringBuffer sBuf = new StringBuffer();
		List<String> value = pair.getValue();
		
		sBuf.append(pair.getKey());
		sBuf.append("  ===> [");
		for(int i = 0; i < value.size() - 1; i++){
			sBuf.append(value.get(i));
			sBuf.append(", ");
		}
		sBuf.append(value.get(value.size() - 1));
		sBuf.append("]");
		System.out.println(sBuf.toString());
	}
}
